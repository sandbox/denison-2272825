<?php

/**
 * @file
 * Page callbacks for the Activity Stream Instagram module.
 */

/**
 * Pull data from a instagram feed.
 */
function _activitystream_instagram_pull_items($credential) {

  // Query need client_id or access_token
  $query = array(
    'client_id' => $credential['client_id'],
    'count' => $credential['count'],
  );

  $url = ACTIVITYSTREAM_INSTAGRAM_URL_API . '/v1/tags/' . $credential['hashtag'] . '/media/recent?' . http_build_query($query);
  $request = drupal_http_request($url);
  $instagram_data = json_decode($request->data, TRUE);

  $items = array();
  if (isset($instagram_data['data'])) {

    foreach ($instagram_data['data'] as $item) {
      $items[] = array(
        'title'     => 'Instagram: ' . $item['id'],
        'body'      => _activitystream_instagram_remove_emoji($item['caption']['text']),
        'link'      => $item['link'],
        'guid'      => $item['id'],
        'raw'       => json_encode($item),
        'timestamp' => $item['created_time'],
      );
    }
  }

  return $items;
}

/**
 * Remove the emoji.
 */
function _activitystream_instagram_remove_emoji($text) {

  return preg_replace('/([0-9|#][\x{20E3}])|[\x{00ae}|\x{00a9}|\x{203C}|\x{2047}|\x{2048}|\x{2049}|\x{3030}|\x{303D}|\x{2139}|\x{2122}|\x{3297}|\x{3299}][\x{FE00}-\x{FEFF}]?|[\x{2190}-\x{21FF}][\x{FE00}-\x{FEFF}]?|[\x{2300}-\x{23FF}][\x{FE00}-\x{FEFF}]?|[\x{2460}-\x{24FF}][\x{FE00}-\x{FEFF}]?|[\x{25A0}-\x{25FF}][\x{FE00}-\x{FEFF}]?|[\x{2600}-\x{27BF}][\x{FE00}-\x{FEFF}]?|[\x{2900}-\x{297F}][\x{FE00}-\x{FEFF}]?|[\x{2B00}-\x{2BF0}][\x{FE00}-\x{FEFF}]?|[\x{1F000}-\x{1F6FF}][\x{FE00}-\x{FEFF}]?/u', '', $text);
}

/**
 * Format date.
 *
 * If the difference between the $created_date and $current_date
 * hours are the hours will be returned. If less than one hour
 * will be returned minutes. If greater than one day be returned the
 * date of creation.
 *
 * @param object $created_date
 *   The DateTime object of creation stream.
 *
 * @return string
 *   The string with the date formatted.
 */
function _activitystream_instagram_diff_date($created_date) {

  $current_date = new DateTime();
  $diff_date = $created_date->diff($current_date);

  if ($diff_date->h >= 23 || $diff_date->d != 0) {
    return $created_date->format('d ' . t('\o\f') . ' F');
  }
  elseif ($diff_date->h == 0) {
    return $diff_date->i . 'min';
  }
  else {
    return $diff_date->h . 'h';
  }
}
