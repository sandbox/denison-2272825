<?php

/**
 * @file
 * Instagram template implementation for an Activity Stream item.
 *
 * Available variables:
 * - $profile: An array of information about the user.
 * - $picture_source: An array of information about the picture.
 * - $video_source: An array of information about the video.
 */
?>

<div class="activitystream-wrapper">
  <div class="activitystream-instagram">

    <div class="activitystream-item-profile">
      <div class="image-profile">
        <?php print render($profile['image']); ?>
      </div>
      <div class="name-profile">
        <?php print render($profile['name']); ?>
      </div>

      <div class="created-date">
        <?php print render($profile['created_date']); ?>
      </div>
    </div>

    <div class="activitystream-item-content">
      <?php if(isset($picture_source)) : ?>
        <span class="caption">
            <?php print $picture_source['caption']; ?>
        </span>

        <div class="picture-source">
          <?php print render($picture_source['img']); ?>
        </div>
      <?php endif;?>

      <?php if(isset($video_source)) : ?>
        <span class="caption">
          <?php print $video_source['caption']; ?>
        </span>
        <div class="video">
          <video controls>
            <source src=<?php print $video_source['video']; ?>>
          </video>
        </div>
      <?php endif;?>
    </div>
  </div>
</div>
