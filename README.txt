
CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------
This module is part of the suite of services Activitystream module. The
Activitystream Instagram service provides integration with Instagram API to
authentication.

Through the administrative interface you can manage
the keys provided by Instagram API and the hash tag where the activities
will be imported.


REQUIREMENTS
------------
This module requires the following modules:
 * Activity Stream (https://drupal.org/project/activitystream)


INSTALLATION
------------
* Install as you would normally install a contributed drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7


CONFIGURATION
-------------
* After register your application in (http://instagram.com/developer/register/)
  go configure it at user/#/edit/activity-stream the following fields:
   - Instagram hashtag: Desired hash tag;
   - Count posts: Number of streams to be imported;
   - Client ID: Provided Instagram API;
   - Client Secret: Provided Instagram API;

* To import the streams run cron drupal.

* View the site-wide activity stream at /activity-stream.

* View the per-user activity stream at /user/#/activity-stream.


MAINTAINERS
-----------
Current maintainers:
 * Denis Souza (denison) - https://drupal.org/user/1669786
